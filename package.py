name = "pyside"
version = "1.0.9"
authors = ["qt-project"]
description = "Qt python binding."
help = "file://$BD_SOFTS/pyside/{version}/docs/index.html"
uuid = "0475198d-2cf5-4532-9c5d-245db6d845e4"

def commands():
    env.PYTHONPATH.append("$BD_SOFTS/pyside/{version}")
    env.LD_LIBRARY_PATH.append("$BD_SOFTS/pyside/{version}/lib")
